# Scandinavian Archaea Diversity

This is the code that we used for parallel high-resolution small subunit rRNA gene sequencing approach to explore archaeal diversity in 109 Swedish lakes, and reveal archaeal community assembly mechanisms.

It includes the bash and R code for processing the raw data as well as performing statistical analyses and visualization for the paper.

20191020_phyloseq_statistics_100lakes.R    r code performing all statistical analysis and visualization

run_dada2_abel.slurm	slurm script for running dada2

runscript_dada2.sh	bash scripts to run the raw data processing

dada2_abel.R	r code used for running dada2

100_lakes_metadata_final.csv    metadata assoicated with the 109 lakes
