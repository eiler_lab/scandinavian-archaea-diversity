
######################################
### Archaea hundred_lakes project

### Use freebio to run cutadapt

## Illumina run 160829_M00485_0295_000000000-AU5KD

# transfer files to scratch

mkdir /work/users/alexaei/input
mkdir /work/users/alexaei/output

rsync -avc --progress /usit/abel/u1/alexaei/01_raw_data/02_hundred_lakes/160829_M00485_0295_000000000-AU5KD/*/*.fastq.gz /work/users/alexaei/input/.

mkdir /work/users/alexaei/databases
mkdir /work/users/alexaei/databases/silva
cd /work/users/alexaei/databases/silva
wget https://zenodo.org/record/1172783/files/silva_nr_v132_train_set.fa.gz

##############################################
### run cutadapt to remove primer sequences
##############################################
# START TMUX
tmux

#start processes insiede the started tmux session

DATADIR=/work/users/alexaei #Root of all data
DIRS=$DATADIR/input              #All directories in which to look for data files
mkdir -p $DATADIR/output/AdaptersRemoved       #Creating directory for new fastq files

for f in $DIRS/*.fastq.gz
    do
        FILE=${f#$DIRS}         #Removing directory form file name
        if [[ $FILE =~ R1 ]] #If file name contains R1
        then
            cutadapt -g NNNNCCCTAYGGGGYGCASCAG -o $DATADIR/output/AdaptersRemoved/$FILE $f    #Removing second half of primer 340f
        elif [[ $FILE =~ R2 ]]
        then
            cutadapt -g GGCCATGCACYWCYTCTC -o $DATADIR/output/AdaptersRemoved/$FILE $f       #Removing second half of primer 1000r
        fi
    done

# detach from tmux session by typing Ctrl + b and then d

# if you want multiple sessions running in parallel you should name the session by Ctrl + b and $

# one can list all running sessions with tmux list-session

# move files to abel for further processing with dada2
mkdir ~/nobackup/01_data/02_hundred_lakes
mkdir ~/nobackup/01_data/02_hundred_lakes/archaea
rsync -avc --progress /work/users/alexaei/output/* ~/nobackup/01_data/02_hundred_lakes/archaea
rm -r /work/users/alexaei/input/*
rm -r /work/users/alexaei/output/*

### login to abel

# move files to abel work folder for further processing with dada2

mkdir /work/users/alexaei/output/archaea
rsync -avc --progress /usit/abel/u1/alexaei/nobackup/01_data/02_hundred_lakes/archaea/AdaptersRemoved /work/users/alexaei/output/archaea/.

## run dada2 interactive in R
#qlogin --account=uio --ntasks=1
#
#module load R
#
#R

# or submit to slurm queue

sbatch /scripts/projects/02_hundred_lakes/archaea/run_dada2_abel.slurm

mv /work/users/alexaei/output/*.pdf /usit/abel/u1/alexaei/nobackup/01_data/02_hundred_lakes/archaea/.
mv /work/users/alexaei/output/*.rds /usit/abel/u1/alexaei/nobackup/01_data/02_hundred_lakes/archaea/.

